#ifndef __LoopSubdivision_h__
#define __LoopSubdivision_h__
#include <unordered_map>
#include <vector>
#include "Mesh.h"

Vector2i Edge_Pair(int a, int b) {
	if (a <= b) return Vector2i(a, b);
	else return Vector2i(b, a);
}

inline void LoopSubdivision(TriangleMesh<3>& mesh)
{
	std::vector<Vector3>& old_vtx=mesh.Vertices();
	std::vector<Vector3i>& old_tri=mesh.Elements();
	std::vector<Vector3> new_vtx;		////vertex array for the new mesh
	std::vector<Vector3i> new_tri;	////element array for the new mesh
	
	new_vtx=old_vtx;	////copy all the old vertices to the new_vtx array

	////step 1: add mid-point vertices and triangles
	////for each old triangle, 
	////add three new vertices (in the middle of each edge) to new_vtx 
	////add four new triangles to new_tri

	std::unordered_map<Vector2i, std::vector<int> > edge_tri_map;
	std::unordered_map<Vector2i, int> edge_vtx_map;
	std::unordered_map<int, std::set<int> > vtx_vtx_map;

	for (int i = 0; i < old_tri.size(); i++) {
		int v1 = old_tri[i][0], v2 = old_tri[i][1], v3 = old_tri[i][2];

		//fill edge_tri_map
		Vector2i e1 = Edge_Pair(v1, v2), e2 = Edge_Pair(v2, v3), e3 = Edge_Pair(v3, v1);
		edge_tri_map[e1].push_back(i); edge_tri_map[e2].push_back(i); edge_tri_map[e3].push_back(i);

		//update new points and fill edge_vtx_map
		Vector3 p1 = old_vtx[v1], p2 = old_vtx[v2], p3 = old_vtx[v3];
		if (!edge_vtx_map.count(e1)) { int m1 = new_vtx.size(); new_vtx.push_back((p1 + p2) * 0.5); edge_vtx_map[e1] = m1; }
		if (!edge_vtx_map.count(e2)) { int m2 = new_vtx.size(); new_vtx.push_back((p2 + p3) * 0.5); edge_vtx_map[e2] = m2; }
		if (!edge_vtx_map.count(e3)) { int m3 = new_vtx.size(); new_vtx.push_back((p3 + p1) * 0.5); edge_vtx_map[e3] = m3; }

		//fill vtx_vtx_map
		vtx_vtx_map[v1].insert(v2); vtx_vtx_map[v1].insert(v3);
		vtx_vtx_map[v2].insert(v1); vtx_vtx_map[v2].insert(v3);
		vtx_vtx_map[v3].insert(v1); vtx_vtx_map[v3].insert(v2);
	}

	//update new triangles
	for (int i = 0; i < old_tri.size(); i++) {
		int v1 = old_tri[i][0], v2 = old_tri[i][1], v3 = old_tri[i][2];
		Vector2i e1 = Edge_Pair(v1, v2), e2 = Edge_Pair(v2, v3), e3 = Edge_Pair(v3, v1);
		int m1 = edge_vtx_map[e1], m2 = edge_vtx_map[e2], m3 = edge_vtx_map[e3];
		new_tri.push_back(Vector3i(v1, m1, m3));
		new_tri.push_back(Vector3i(m1, v2, m2));
		new_tri.push_back(Vector3i(m1, m2, m3));
		new_tri.push_back(Vector3i(m3, m2, v3));
	}


	////step 2: update the position for each new mid-point vertex: 
	////for each mid-point vertex, find its two end-point vertices A and B, 
	////and find the two opposite-side vertices on the two incident triangles C and D,
	////then update the new position as .375*(A+B)+.125*(C+D)

	for (auto iter = edge_vtx_map.begin(); iter != edge_vtx_map.end(); iter++) {
		Vector2i e = iter->first; int v = iter->second;
		int A = e[0], B = e[1];
		auto tri_vec = edge_tri_map[e];
		if (tri_vec.size() == 2) {
			Vector3i t1 = old_tri[tri_vec[0]], t2 = old_tri[tri_vec[1]];
			int C; for (int k = 0; k < 3; k++) { if (t1[k] != A && t1[k] != B) C = t1[k]; }
			int D; for (int k = 0; k < 3; k++) { if (t2[k] != A && t2[k] != B) D = t2[k]; }
			new_vtx[v] = 0.375 * (old_vtx[A] + old_vtx[B]) + 0.125 * (old_vtx[C] + old_vtx[D]);
		}
	}

	////step 3: update vertices of each old vertex
	////for each old vertex, find its incident old vertices, and update its position according its incident vertices

	for (int v = 0; v < old_vtx.size(); v++) {
		const auto& nbs = vtx_vtx_map[v];
		int n = nbs.size();
		double beta;
		if (n == 3) beta = 3.0 / 16.0;
		else if (n > 3) beta = 3.0 / (8.0 * n);
		else continue;
		Vector3 pos = Vector3::Zero();
		for (auto iter = nbs.begin(); iter != nbs.end(); iter++) pos += beta * old_vtx[*iter];
		pos += (1.0 - n * beta) * new_vtx[v];
		new_vtx[v] = pos;
	}

	////update subdivided vertices and triangles onto the input mesh
	old_vtx=new_vtx;
	old_tri=new_tri;
}

#endif