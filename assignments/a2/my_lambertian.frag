/*This is your first fragment shader!*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/* Passed time from the begining of the program */ 
uniform float iTime;

/*input variables*/
in vec3 vtx_pos;
in vec3 vtx_normal;
in vec4 vtx_color;
////TODO [Step 2]: add your in variables from the vertex shader

/*output variables*/
out vec4 frag_color;

/*hard-coded lighting parameters*/
const vec3 LightPosition = vec3(3, 1, 3);
////TODO [Step 2]: add your Lambertian lighting parameters here

void main()							
{		
	////TODO [Step 2]: add your Lambertian lighting calculation
	float ka=0.05;
	float kd=0.95;

	vec3 dir=normalize(LightPosition-vtx_pos);
	vec4 light_color=vtx_color;

	vec4 ambient=ka*vtx_color;
	vec4 directional=kd*light_color*max(0,dot(vtx_normal,dir));

	frag_color=ambient+directional;
}	