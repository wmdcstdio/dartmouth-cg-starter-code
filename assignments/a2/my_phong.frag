/*This is your first fragment shader!*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/* Passed time from the begining of the program */ 
uniform float iTime;

/*input variables*/
in vec3 vtx_pos;
in vec3 vtx_normal;
in vec4 vtx_color;
////TODO [Step 2]: add your in variables from the vertex shader

/*output variables*/
out vec4 frag_color;

/*hard-coded lighting parameters*/
const vec3 LightPosition = vec3(3, 1, 3);
////TODO [Step 2]: add your Lambertian lighting parameters here

void main()							
{		
	////TODO [Step 2]: add your Lambertian lighting calculation
	float ka=0.05;
	float kd=0.8;
	float ks=10.0;
	float kp=20.0;

	vec3 light_dir=normalize(LightPosition-vtx_pos);
	vec3 view_dir=normalize(position.xyz-vtx_pos);
	vec3 reflect_dir=reflect(-light_dir,vtx_normal);
	vec4 light_color=vtx_color;
	vec4 specular_color=vec4(1.f,1.f,1.f,1.f);

	vec4 ambient=ka*vtx_color;
	vec4 diffuse=kd*light_color*max(0,dot(vtx_normal,light_dir));
	vec4 specular=ks*specular_color*pow(max(0,dot(view_dir,reflect_dir)),kp);

	frag_color=ambient+diffuse+specular;
}	