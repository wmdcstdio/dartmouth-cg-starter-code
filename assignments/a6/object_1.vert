/*This is your first vertex shader!*/

#version 330 core

#define PI 3.14159265

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
layout (location=0) in vec4 pos;		/*vertex position*/
layout (location=1) in vec4 color;		/*vertex color*/
layout (location=2) in vec4 normal;		/*vertex normal*/
layout (location=3) in vec4 uv;			/*vertex uv*/
layout (location=4) in vec4 tangent;	/*vertex tangent*/

uniform mat4 model;						////model matrix
uniform float iTime;					////time

//mat4 model=mat4(
//0.05f,0.f,0.f,0.f,		////column 0
//0.f,0.05f,0.f,0.f,		////column 1
//0.f,0.f,0.05f,0.f,		////column 2
//0.f, 0.f, 0.f, 1.f);		////column 3	////set the translation in the last column



mat4 rotationMatrix(vec3 axis, float angle)
{
	axis = normalize(axis);
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;

	return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
	oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
	oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
	0.0,                                0.0,                                0.0,                                1.0);
}


/*output variables*/
out vec3 vtx_pos;		////vertex position in the world space
out vec3 vtx_normal;
out vec2 vtx_uv;
out vec3 vtx_tang;

void main()
{
	//model = rotationMatrix(vec3(0.f, 0.f, 1.0f), iTime * radians(180.f)) * model;

	vtx_pos= (pos).rgb;
	vtx_normal=normalize(normal.xyz);
	vtx_tang=normalize(tangent.xyz);
	vtx_uv = uv.xy;
	//	vtx_pos = pos.xyz * height(pos.xyz);
	gl_Position=pvm * model * vec4(vtx_pos.xyz,1.f);
}
