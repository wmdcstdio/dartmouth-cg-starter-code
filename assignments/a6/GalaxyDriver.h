#ifndef __GalaxyDriver_h__
#define __GalaxyDriver_h__
#include "Galaxy.h"
#include "OpenGLCommon.h"

template<int d>
class GalaxyDriver {
	Typedef_VectorDii(d);
public:
	int number = 1000;
	real init_radius = 30;
	real min_star_radius = 1.0;//1x~2x. 1x correspond to mass=1
	real max_rate;//max initial velocity
	real G;
	int iter = 0;
	Galaxy<d> galaxy_prev;
	Galaxy<d> galaxy;
	Galaxy<d> galaxy_next;
	real Clamp(real x) {
		if (x < 0) return 0;
		if (x > 1) return 1;
		return x;
	}
	OpenGLColor Color(real rate) {
		real vPortion = sqrt(rate / max_rate);
		OpenGLColor c;
		c.rgba[0] = Clamp(4 * (vPortion - 0.333));
		c.rgba[1] = Clamp(min(4 * vPortion, 4.0 * (1.0 - vPortion)));
		c.rgba[2] = Clamp(4 * (0.5 - vPortion));
		c.rgba[3] = 1.0;
		return c;
	}
	void Initialize(void);
	void Advance_Step(real dt) {
		galaxy_next = galaxy;
		if (iter == 0) {
			galaxy.Calc_Acceleration(G);
			galaxy.Calc_Euler_Velocity(galaxy, 0.5 * dt);
		}
		galaxy_next.Update_Leapfrog(galaxy, dt);
		iter++;
		galaxy_prev = galaxy;
		galaxy = galaxy_next;
	}
};

#endif

