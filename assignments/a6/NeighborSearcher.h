//////////////////////////////////////////////////////////////////////////
// Common header
// Copyright (c) (2018-), Mengdi Wang
// This file is part of SimpleX, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#ifndef __NeighborSearcher_h__
#define __NeighborSearcher_h__

#include "Common.h"
#include "nanoflann.hpp"
#include <iostream>

//// Usage:
//// Initialize (only once) -> Update_Points(frame_1_points) -> queries -> Update_Points(frame_2_points) -> ...
template<int d>
class NeighborSearcher {
	Typedef_VectorDii(d);
	using FilterFunc = std::function<bool(const int)>;
public:
	Array<int> search_results;
public:
	// Create and initialize with NO points
	NeighborSearcher(){}

	//// A Child-Class must implement the following functions:
	// Re-build data structure with points
	virtual void Build_Data(Array<VectorD>& points) = 0;

	// NOTE: all query functions except for Record_Neighbors should be thread safe. If not, please refer to Mengdi.

	// Store results in Array<int>&results. Clear former results when append==false
	virtual size_t Find_Neighbors(const VectorD& pos, const real& radius, Array<int>& results, bool append = false)const = 0;
	virtual int Find_Nearest_Nb(const VectorD& pos)const = 0;
	virtual int Find_K_Nearest_Nb(const VectorD& pos, int k, Array<int>& results)const = 0;


public:

	//// Other interfaces:

	// Clear and load points, used for every time step when points are updated
	void Update_Points(Array<VectorD>& points);
	void Update_Points(Array<VectorD>& points, FilterFunc& filter_func);

	//Search points in radius and with filter_func()==true
	size_t Find_Neighbors(const VectorD& pos, const real& radius, FilterFunc& filter_func, Array<int>& results, bool append = false)const;
	//Pass-by-value style, more convenient for use
	Array<int> Find_Neighbors(const VectorD& pos, const real& radius)const;
	Array<int> Find_Neighbors(const VectorD& pos, const real& radius, FilterFunc& filter_func)const;
};

template<int d>
class PointSetAdapter {
	//Declare_Eigen_Types(double, d);
	Typedef_VectorDii(d);
public:
	std::shared_ptr<Array<VectorD> > points_ptr;
	void Initialize(Array<VectorD>& arr);
	// Interface required by nanoflann
	size_t kdtree_get_point_count() const;
	real kdtree_get_pt(const size_t idx, const size_t dim) const;
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }
};

template<int d>
class NeighborKDTree: public NeighborSearcher<d> {
	//Declare_Eigen_Types(double, d);
	using Base = NeighborSearcher<d>;
	Typedef_VectorDii(d);
public:
	const int max_leaf = 10;/* max leaf */
	PointSetAdapter<d> points;
	using my_kd_tree_t = nanoflann::KDTreeSingleIndexAdaptor<
		nanoflann::L2_Simple_Adaptor<real, PointSetAdapter<d> >,//NOTE: It's actually squared L2 norm
		PointSetAdapter<d>,
		d /* dim */
	>;
	my_kd_tree_t index;
public:
	NeighborKDTree() :index(d, points, nanoflann::KDTreeSingleIndexAdaptorParams(max_leaf)) { index.buildIndex(); }
	virtual void Build_Data(Array<VectorD>& arr);
	virtual size_t Find_Neighbors(const VectorD& pos, const real& radius, Array<int>& results, bool append = false)const;
	virtual int Find_Nearest_Nb(const VectorD& pos)const;
	virtual int Find_K_Nearest_Nb(const VectorD& pos, int k, Array<int>& results)const;
};

#endif
