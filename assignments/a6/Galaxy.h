#ifndef __Galaxy_h__
#define __Galaxy_h__
#include "Common.h"
#include "NeighborSearcher.h"

template<int d>
class Galaxy {
	Typedef_VectorDii(d);
public:
	int n;
	real G;
	real avg_nearest;
	Array<real> mass;
	Array<VectorD> pos;
	Array<VectorD> vel;//vel step i+dt/2
	Array<VectorD> acc;
	Array<real> radius;
	VectorD Grav_Acc(const real& G, const real& m, const VectorD& r) {
		real len = r.norm();
		real eps = 0.1;
		return G * m * (-r) / ((len * len + eps * eps) * len);
	}
	void Calc_Acceleration(real G) {
		NeighborKDTree<d> nbs_searcher;
		nbs_searcher.Update_Points(pos);
		real search_radius = avg_nearest * 10;
		Array<int> nbs;
		for (int i = 0; i < n; i++) {
			acc[i] = VectorD::Zero();
			nbs_searcher.Find_Neighbors(pos[i], search_radius, nbs);
			for (int idx = 0; idx < nbs.size(); idx++) {
				int j = nbs[idx];
				if (i == j) continue;
				acc[i] += Grav_Acc(G, mass[j], pos[i] - pos[j]);
			}
		}
	}
	void Update_Leapfrog(const Galaxy& glx_1, real dt) {
		//glx_1: pos step i, vel step i+1/2
		for (int i = 0; i < n; i++) {
			pos[i] = glx_1.pos[i] + dt * glx_1.vel[i];
		}
		Calc_Acceleration(G);
		for (int i = 0; i < n; i++) {
			vel[i] = glx_1.vel[i] + dt * acc[i];
		}
	}
	void Calc_Euler_Velocity(const Galaxy& glx_1, real dt) {
		//glx_1: step i
		//*this: step i+1
		for (int i = 0; i < n; i++) {
			vel[i] = glx_1.vel[i] + dt * glx_1.acc[i];
			//pos[i] = glx_1.pos[i] + dt * glx_1.vel[i];
		}
	}
};

#endif