/*This is your fragment shader for texture and normal mapping*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

in vec3 vtx_pos;		////vertex position in the world space
in vec3 vtx_normal;
in vec2 vtx_uv;
in vec3 vtx_tang;

//  Simplex 3D Noise
//  by Ian McEwan, Ashima Arts
//
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}

float snoise(vec3 v){
	const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
	const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

	// First corner
	vec3 i  = floor(v + dot(v, C.yyy) );
	vec3 x0 =   v - i + dot(i, C.xxx) ;

	// Other corners
	vec3 g = step(x0.yzx, x0.xyz);
	vec3 l = 1.0 - g;
	vec3 i1 = min( g.xyz, l.zxy );
	vec3 i2 = max( g.xyz, l.zxy );

	//  x0 = x0 - 0. + 0.0 * C
	vec3 x1 = x0 - i1 + 1.0 * C.xxx;
	vec3 x2 = x0 - i2 + 2.0 * C.xxx;
	vec3 x3 = x0 - 1. + 3.0 * C.xxx;

	// Permutations
	i = mod(i, 289.0 );
	vec4 p = permute( permute( permute(
	i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
	+ i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
	+ i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

	// Gradients
	// ( N*N points uniformly over a square, mapped onto an octahedron.)
	float n_ = 1.0/7.0; // N=7
	vec3  ns = n_ * D.wyz - D.xzx;

	vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

	vec4 x_ = floor(j * ns.z);
	vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

	vec4 x = x_ *ns.x + ns.yyyy;
	vec4 y = y_ *ns.x + ns.yyyy;
	vec4 h = 1.0 - abs(x) - abs(y);

	vec4 b0 = vec4( x.xy, y.xy );
	vec4 b1 = vec4( x.zw, y.zw );

	vec4 s0 = floor(b0)*2.0 + 1.0;
	vec4 s1 = floor(b1)*2.0 + 1.0;
	vec4 sh = -step(h, vec4(0.0));

	vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
	vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

	vec3 p0 = vec3(a0.xy,h.x);
	vec3 p1 = vec3(a0.zw,h.y);
	vec3 p2 = vec3(a1.xy,h.z);
	vec3 p3 = vec3(a1.zw,h.w);

	//Normalise gradients
	vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
	p0 *= norm.x;
	p1 *= norm.y;
	p2 *= norm.z;
	p3 *= norm.w;

	// Mix final noise value
	vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
	m = m * m;
	return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3) ) );
}

float noiseOctave(vec3 v, int num)
{
	float minValue = 1.f;
	float strength = 0.07f;
	float sum = 0;
	float scale=1, weight=1;
	for(int i=0;i<num;i++){
		sum+=(snoise(v*scale)+1.f) * 0.5f *weight;
		scale*=2;
		weight*=0.5;
	}
	sum = max(0, sum - 1.f);
	sum *= strength;
	return sum;
}

float ridgedNoise(vec3 point, int numLayers)
{
	float noiseValue = 0;
	float frequency = 1.59;
	float amplitude = 1;
	float weight = 1;

	for (int i = 0; i < numLayers; i++)
	{
		float v = 1-abs(snoise(point * frequency));
		v *= v;
		v *= weight;
		weight = clamp(v * 0.78, 0, 1);

		noiseValue += v * amplitude;
		frequency *= 3.3;
		amplitude *= 0.5;
	}
	noiseValue = max(0, noiseValue - 0.37);
	return noiseValue * 1.42;
}

float height(vec3 v){
	float h = 0;
	// Your implementation starts here
	float h1 = noiseOctave(v, 4);
	h = 1. + ridgedNoise(v, 4) * h1;
	// Your implementation ends here
	return h;
}


vec3 biome(float e) {
	//	if (e < 0.1) return vec3(212./255., 241./255., 249./255.);
	vec3 c;
	if (e < 0.001)  c = vec3(121./255., 30./255., 2./255.) ;
	else if (e < 0.07) c = vec3(236./255., 191./255., 130./255.);
	else if (e < 0.1) c = vec3(11./255, 102./255., 35./255.);
	else if (e < 0.13) c = vec3(30./255., 56./255., 33./255.);
	else if (e < 0.17) c = vec3(88./255., 93./255., 79./255.);
	else if (e < 0.2) c = vec3(237./255., 201./255., 175./255.);
	else c = vec3(1., 250./255., 250./255.);
	return c;
}

/*This part is the same as your previous assignment. Here we provide a default parameter set for the hard-coded lighting environment. Feel free to change them.*/
const vec3 LightPosition = vec3(3, 1, 3);
const vec3 LightIntensity = vec3(20);
const vec3 ka = 0.1*vec3(1., 1., 1.);
const vec3 kd = 0.7*vec3(1., 1., 1.);
const vec3 ks = vec3(2.);
const float n = 400.0;

vec3 get_color(vec3 v) {
	float h= height(v) - 1;

	vec3 dir=normalize(LightPosition-vtx_pos);
	vec4 texture_col=texture(tex_albedo, vtx_uv);
//	vec3 normal = texture(tex_normal, vtx_uv).rgb;
//	normal = normalize(normal * 2.0 - 1.0);
//	vec3 vtx_bitang=cross(vtx_normal, vtx_tang);
//	mat3 TBN=mat3(vtx_tang,vtx_bitang,vtx_normal);
//	////Step 3.3: transform the texture normal from the local tangent space to the global world space
//	normal=normalize(TBN*normal);
	////Step 3.4 and following: use the transformed normal and the loaded texture color to conduct the further lighting calculation
//	vec4 shading_col = vec4(ka+kd*max(0,dot(normal,dir)),1.0);
	vec4 shading_col = vec4(ka+kd*max(0,dot(vtx_normal,dir)),1.0);
	////The way to declare a 3x3 matrix is mat3 mat=mat3(v0,v1,v2);
	////The way to read a texture is to call texture(texture_name,uv). It will return a vec4.
	////The way to calculate cross product is to call cross(u1,u2);
//	vec4 col=texture_col*shading_col;
//	frag_color = vec4(col.rgb, 1);
	return biome(h) * shading_col.rgb;
//	return biome(h);
//	r: 0.83116925, g: 0.8490566, b: 0.40450338, a: 1}
//key1: {r: 0.5158609, g: 0.7058824, b: 0.090196066, a: 1}
//key2: {r: 0.23555952, g: 0.4627451, b: 0.019607844, a: 0}
//key3: {r: 0.19269368, g: 0.4528302, b: 0, a: 0}
//key4: {r: 0.5943396, g: 0.36861122, b: 0.21026166, a: 0}
//key5: {r: 0.38679248, g: 0.2116821, b: 0.09669812, a: 0}
//key6: {r: 0.3207547, g: 0.14127098, b: 0, a: 0}
//key7: {r: 0.3207547, g: 0.14127098, b: 0, a: 0}
}

out vec4 frag_color;

void main()
{
	frag_color = vec4(get_color(vtx_pos), 1.);
}