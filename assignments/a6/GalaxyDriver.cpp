#include "GalaxyDriver.h"
#include <iostream>
#include <numeric>


real random_real(void) {//random real number in [0,1)
	int mask = ((1 << 15) - 1);
	int num = (rand() & mask);
	num = (num << 15) | (rand() & mask);
	int mx_num = ((mask << 15) | mask);
	return (num + 0.0) / mx_num;
}

real random_real(real a, real b) {//random in [a,b)
	return random_real() * (b - a) + a;
}

template<int d>
void GalaxyDriver<d>::Initialize(void)
{
	real density = 1.0 / pow(min_star_radius, 3);
	real pi = acos(-1.0);
	for (int i = 0; i < 1000; i++) rand();
	VectorD up(0, 1, 0);
	galaxy.n = number;
	for (int i = 0; i < number; i++) {
		real r_p = random_real(0.1, 1.0) * init_radius;
		real theta = random_real() * 2 * pi;
		VectorD pos(r_p * cos(theta), 0, r_p * sin(theta));
		real r = random_real(1, 2) * min_star_radius;
		real mass = density * pow(r, 3);
		galaxy.mass.push_back(mass);
		galaxy.pos.push_back(pos);
		galaxy.acc.push_back(VectorD::Zero());
		galaxy.radius.push_back(r);
	}
	real m_sum = std::accumulate(galaxy.mass.begin(), galaxy.mass.end(), 0.0);
	G = 0.02 / pow(0.1 * init_radius, 2);
	galaxy.G = G;

	NeighborKDTree<d> nbs_searcher;
	nbs_searcher.Update_Points(galaxy.pos);
	real avg_nl = 0;
	for (int i = 0; i < number; i++) {
		Array<int> ary; nbs_searcher.Find_K_Nearest_Nb(galaxy.pos[i], 2, ary);
		int j = (ary[0] == i) ? ary[1] : ary[0];
		real nb_len = (galaxy.pos[i] - galaxy.pos[j]).norm();
		avg_nl += nb_len;
	}
	avg_nl /= number;
	galaxy.avg_nearest = avg_nl;
	/*G = 0.001 / (avg_nl * avg_nl);
	galaxy.G = G;*/
	max_rate = 0.0;
	for (int i = 0; i < number; i++) {
		VectorD pos = galaxy.pos[i];
		real r = pos.norm();
		real vel_rate = sqrt(G / r * (m_sum + m_sum * (r - 0.1 * init_radius) / (0.9 * init_radius))) * 1.0;
		//real vel_rate = max_init_vel * random_real(0.9, 1.1) * pos.norm();
		VectorD vel = up.cross(pos.normalized()) * vel_rate;
		galaxy.vel.push_back(vel);
		max_rate = max(max_rate, vel.norm());
	}
	galaxy.mass.push_back(m_sum);
	galaxy.pos.push_back(VectorD::Zero());
	galaxy.acc.push_back(VectorD::Zero());
	galaxy.radius.push_back(0);
	galaxy.vel.push_back(VectorD::Zero());
}


template class GalaxyDriver<3>;