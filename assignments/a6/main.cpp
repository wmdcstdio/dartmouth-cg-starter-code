//#####################################################################
// Main
// Dartmouth COSC 77/177 Computer Graphics, starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################
#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include "Common.h"
#include "Driver.h"
#include "OpenGLMesh.h"
#include "OpenGLCommon.h"
#include "OpenGLWindow.h"
#include "OpenGLViewer.h"
#include "OpenGLMarkerObjects.h"
#include "TinyObjLoader.h"
#include <gtx/string_cast.hpp>
#include "GalaxyDriver.h"


#ifndef __Main_cpp__
#define __Main_cpp__

#ifdef __APPLE__
#define CLOCKS_PER_SEC 100000
#endif

class FinalProjectDriver : public Driver, public OpenGLViewer
{using Base=Driver;
	std::vector<OpenGLTriangleMesh*> mesh_object_array;						////mesh objects, every object you put in this array will be rendered.
	clock_t startTime;
	OpenGLTriangleMesh* spacecraft;
    OpenGLTriangleMesh* marsMesh;
    OpenGLTriangleMesh* sunMesh;
	GalaxyDriver<3> galaxy_driver;
	int FPS = 25;
	float spaceship_scale = 1000;
	float spaceship_down_len = 2.8;
	float spaceship_forward_len = 4.0;
	float spaceship_forward_start = 0.0, spaceship_forward_end = 0.0;
	float spaceship_intenseforward_start = 0.0, spaceship_intenseforward_end = 80.0;
	float spaceship_last_start = 0.0, spaceship_last_end = 8.0;
	float spaceship_lastright_start = 0.0, spaceship_lastright_end = 1.5;
	float spaceship_up_start = 0.0, spaceship_up_end = 2.0;
	int galaxy_start_idx = 0;
	float up_time = 5.0;
	float stay_time = 1.0;
	float time_1 = 20;
	float time_2 = 5;
	float time_3 = 5;
	float time_4 = 30;

public:
	virtual void Initialize()
	{
		draw_bk=false;						////turn off the default background and use the customized one
		draw_axes=false;						////if you don't like the axes, turn them off!
		startTime=clock();
		OpenGLViewer::Initialize();
		galaxy_driver.Initialize();
		FinalProjectDriver::Initialize_Data();
	}

	void Add_Shaders()
	{
		////format: vertex shader name, fragment shader name, shader name
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("background.vert","background.frag","background");	
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("object_1.vert","object_1.frag","object_1");		
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("object_2.vert","object_2.frag","object_2");	
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("object_3.vert","object_3.frag","object_3");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("extract.vert", "extract.frag", "extract");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("final.vert", "final.frag", "final");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("blur.vert", "blur.frag", "blur");
	}

	void Add_Textures()
	{
		////format: image name, texture name
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File_32Bit("spacecraft_diffuse.png", "object_1_albedo");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("spacecraft_normal.png", "object_1_normal");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("earth_albedo.png", "object_2_albedo");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("earth_normal.png", "object_2_normal");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("sun.jpeg", "object_3_albedo");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("earth_normal.png", "object_3_normal");

	}

	void Add_Background()
	{
		OpenGLBackground* opengl_background=Add_Interactive_Object<OpenGLBackground>();
		opengl_background->shader_name="background";
		opengl_background->Initialize();
	}

	////this is an example of adding a mesh object read from obj file
	int Add_Object_1()
	{
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();
		spacecraft = mesh_obj;

		////read mesh file
		std::string obj_file_name="spacecraft.obj";
		Array<std::shared_ptr<TriangleMesh<3> > > meshes;
		Obj::Read_From_Obj_File_Discrete_Triangles(obj_file_name,meshes);
		mesh_obj->mesh=*meshes[0];

		////This is an example showing how to access and modify the values of vertices on the CPU end.
		//std::vector<Vector3>& vertices=mesh_obj->mesh.Vertices();
		//int vn=(int)vertices.size();
		//for(int i=0;i<vn;i++){
		//	vertices[i]+=Vector3(1.,0.,0.);
		//}

		////This is an example of creating a 4x4 matrix for GLSL shaders. Notice that the matrix is column-major (instead of row-major!)
		////The code for passing the matrix to the shader is in OpenGLMesh.h
		mesh_obj->model_matrix=
			glm::mat4(0.001f,0.f,0.f,0.f,		////column 0
					  0.f,0.001f,0.f,0.f,		////column 1
					  0.f,0.f,0.001f,0.f,		////column 2
					  0.f,1.f,0.f,1.f);		////column 3	////set the translation in the last column

		////set up shader
		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("object_1"));
		
		////set up texture
		mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("object_1_albedo"));
		mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("object_1_normal"));
		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
		
		////initialize
		mesh_obj->Set_Data_Refreshed();
		mesh_obj->Initialize();	
		mesh_object_array.push_back(mesh_obj);
		return (int)mesh_object_array.size()-1;
	}

	////this is an example of adding a spherical mesh object generated analytically
	int Add_Object_2()
	{
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();
        marsMesh = mesh_obj;
		real radius=1.;
		Initialize_Sphere_Mesh(radius,&mesh_obj->mesh,3);		////add a sphere with radius=1. if the obj file name is not specified
        Translate_Vertex_Position_For_Mesh_Object(mesh_obj, Vector3(-3.0, 0., 0.));
        Update_Vertex_Color_And_Normal_For_Mesh_Object(mesh_obj);
        Update_Vertex_UV_For_Mesh_Object(mesh_obj);
		////set up shader
		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("object_2"));
		
		////set up texture
		mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("object_2_albedo"));
		mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("object_2_normal"));
		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
		
		////initialize
		mesh_obj->Set_Data_Refreshed();
		mesh_obj->Initialize();	
		mesh_object_array.push_back(mesh_obj);
		return (int)mesh_object_array.size()-1;
	}

  int Add_Object_3()
  {

      auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();
      sunMesh = mesh_obj;
      real radius=1.5;
      Initialize_Sphere_Mesh(radius,&mesh_obj->mesh,3);		////add a sphere with radius=1. if the obj file name is not specified
      Translate_Vertex_Position_For_Mesh_Object(mesh_obj, Vector3(3.0, 0., 0.));
      Update_Vertex_Color_And_Normal_For_Mesh_Object(mesh_obj);
      Update_Vertex_UV_For_Mesh_Object(mesh_obj);
      ////set up shader
      mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("object_3"));

      ////set up texture
      mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("object_3_albedo"));
      mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("object_3_normal"));
      Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
      Set_Shading_Mode(mesh_obj,ShadingMode::Texture);

      ////initialize
      mesh_obj->Set_Data_Refreshed();
      mesh_obj->Initialize();
      mesh_object_array.push_back(mesh_obj);
      return (int)mesh_object_array.size()-1;

//		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();
//		auto& mesh=mesh_obj->mesh;
//
//		////vertex position
//		std::vector<Vector3> triangle_vertices={Vector3(-1,-1,-1),Vector3(1,-1,-1),Vector3(-1,-1,1),Vector3(1,-1,1)};
//		std::vector<Vector3>& vertices=mesh_obj->mesh.Vertices();
//		vertices=triangle_vertices;
//
//		////vertex color
//		std::vector<Vector4f>& vtx_color=mesh_obj->vtx_color;
//		vtx_color={Vector4f(1.f,0.f,0.f,1.f),Vector4f(0.f,1.f,0.f,1.f),Vector4f(0.f,0.f,1.f,1.f),Vector4f(1.f,1.f,0.f,1.f)};
//
//		////vertex normal
//		std::vector<Vector3>& vtx_normal=mesh_obj->vtx_normal;
//		vtx_normal={Vector3(0.,1.,0.),Vector3(0.,1.,0.),Vector3(0.,1.,0.),Vector3(0.,1.,0.)};
//
//		////vertex uv
//		std::vector<Vector2>& uv=mesh_obj->mesh.Uvs();
//		uv={Vector2(0.,0.),Vector2(1.,0.),Vector2(0.,1.),Vector2(1.,1.)};
//
//		////mesh elements
//		std::vector<Vector3i>& elements=mesh_obj->mesh.Elements();
//		elements={Vector3i(0,1,3),Vector3i(0,3,2)};
//
//		////set up shader
//		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("object_3"));
//
//		////set up texture
//		mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("object_3_albedo"));
//		mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("object_3_normal"));
//		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
//		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
//
//		////initialize
//		mesh_obj->Set_Data_Refreshed();
//		mesh_obj->Initialize();
//		mesh_object_array.push_back(mesh_obj);
//		return (int)mesh_object_array.size()-1;
  }

	////this is an example of adding an object with manually created triangles and vertex attributes
    void Translate_Vertex_Position_For_Mesh_Object(OpenGLTriangleMesh* obj,const Vector3& translate)
    {
        std::vector<Vector3>& vertices=obj->mesh.Vertices();
        for(auto& v:vertices){
            v+=translate;
        }
    }

  ////This function demonstrates how to manipulate the color and normal arrays of a mesh on the CPU end.
  ////The updated colors and normals will be sent to GPU for rendering automatically.
  void Update_Vertex_Color_And_Normal_For_Mesh_Object(OpenGLTriangleMesh* obj)
  {
      int vn=(int)obj->mesh.Vertices().size();					////number of vertices of a mesh
      std::vector<Vector3>& vertices=obj->mesh.Vertices();		////you might find this array useful
      std::vector<Vector3i>& elements=obj->mesh.Elements();		////you might find this array also useful

      std::vector<Vector4f>& vtx_color=obj->vtx_color;
      vtx_color.resize(vn);
      std::fill(vtx_color.begin(),vtx_color.end(),Vector4f::Zero());

      ////TODO [Step 0]: update the color for each vertex.
      ////NOTICE: This code updates the vertex color array on the CPU end. The array will then be sent to GPU and read it the vertex shader as v_color.
      ////You don't need to implement the CPU-GPU data transfer code.
      for(int i=0;i<vn;i++){
          vtx_color[i]=Vector4f(0.,1.,0.,1.);	////specify color for each vertex
      }

      std::vector<Vector3>& vtx_normal=obj->vtx_normal;
      vtx_normal.resize(vn);
      std::fill(vtx_normal.begin(),vtx_normal.end(),Vector3::Zero());

      //TODO: update the normal for each vertex
      //NOTICE: This code updates the vertex normal array on the CPU end. The array will then be sent to GPU and read it the vertex shader as normal.
      //This is a default implementation of vertex normal that works for a sphere centered around the origin only.
      /*for(int i=0;i<vn;i++){
          vtx_normal[i]=Vector3(vertices[i][0],vertices[i][1],vertices[i][2]);
      }*/

      ////TODO [Step 1]: Comment the default implementation and uncomment the following function and implement it to calculate mesh normals.
      Update_Vertex_Normal(vertices,elements,vtx_normal);
  }

  ////TODO [Step 1]: implement your function to update vertex normals
  void Update_Vertex_Normal(const std::vector<Vector3>& vertices,const std::vector<Vector3i>& elements,std::vector<Vector3>& normals)
  {
      ////TODO [Step 1]: your implementation to calculate the normal vector for each mesh vertex
      normals.resize(vertices.size());
      for (int i = 0; i < normals.size(); i++) normals[i] = Vector3::Zero();
      std::vector<int> adj_num;
      adj_num.resize(vertices.size());
      std::fill(adj_num.begin(), adj_num.end(), 0);
      for (int i = 0; i < elements.size(); i++) {
          const Vector3i& face = elements[i];
          Vector3 a = vertices[face[0]], b = vertices[face[1]], c = vertices[face[2]];
          Vector3 n = (b - a).cross(c - a).normalized();
          for (int k = 0; k < 3; k++) {
              normals[face[k]] += n;
              adj_num[face[k]]++;
          }
      }
      for (int i = 0; i < vertices.size(); i++) {
          normals[i] /= (adj_num[i] + 0.0);
          normals[i].normalize();
      }
  }


  void Update_Vertex_UV_For_Mesh_Object(OpenGLTriangleMesh* obj)
  {
      int vn=(int)obj->mesh.Vertices().size();					////number of vertices of a mesh
      std::vector<Vector3>& vertices=obj->mesh.Vertices();		////you might find this array useful
      std::vector<Vector2>& uv=obj->mesh.Uvs();					////you need to set values in uv to specify the texture coordinates
      uv.resize(vn);
      for(int i=0;i<vn;i++){uv[i]=Vector2(0.,0.);}				////set uv to be zero by default

      Update_Uv_Using_Spherical_Coordinates(vertices,uv);
  }

  void Update_Uv_Using_Spherical_Coordinates(const std::vector<Vector3>& vertices,std::vector<Vector2>& uv)
  {
      /*Your implementation starts*/
      real PI = glm::pi<real>();
      for (int i = 0; i < vertices.size(); i++) {
          Vector3 r = vertices[i];
          real radius = r.norm();
          real u = 0.5 + (atan2(r[0], r[2])) / (2 * PI);
          real v = 0.5 - asin(r[1] / radius) / PI;
          uv[i] = Vector2(u, v);
      }
      /*Your implementation ends*/
  }



	void Add_Galaxy(void);
	void Sync_Galaxy(void);//sync to rendered opengl particles
	void Cancel_Galaxy(void);
	virtual void Initialize_Data()
	{
		Add_Shaders();
		Add_Textures();

		Add_Background();
		Add_Object_1();
		Add_Object_2();
		Add_Object_3();
		Add_Galaxy();
	}

	////Goto next frame 
	virtual void Toggle_Next_Frame()
	{
		for (auto& mesh_obj : mesh_object_array) {
			mesh_obj->setTime(GLfloat(clock() - startTime) / CLOCKS_PER_SEC);
		}
		OpenGLViewer::Toggle_Next_Frame();
	}

	void Update_Scene1(int frame_cnt);
	void Update_Scene2(int frame_cnt);
	void Update_Scene3(int frame_cnt);
	void Update_Scene4(int frame_cnt);

	virtual void Update_Frame();

	virtual void Run()
	{
		OpenGLViewer::Run();
	}
};

int main(int argc,char* argv[])
{
	FinalProjectDriver driver;
	driver.Initialize();
	driver.Run();	
}

void FinalProjectDriver::Add_Galaxy(void)
{
	galaxy_start_idx = opengl_window->object_list.size();
	for (int i = 0; i < galaxy_driver.number; i++) {
		auto pt_obj = Add_Interactive_Object<OpenGLPoint>();
		pt_obj->Set_Data_Refreshed();
		pt_obj->Initialize();
		pt_obj->pos = galaxy_driver.galaxy.pos[i];
	}
}

void FinalProjectDriver::Sync_Galaxy(void)
{
	for (int i = 0; i < galaxy_driver.number; i++) {
		auto pt_obj = dynamic_cast<OpenGLPoint*>((opengl_window->object_list[i + galaxy_start_idx]).get());
		pt_obj->pos = galaxy_driver.galaxy.pos[i];
		//pt_obj->Set_Color(OpenGLColor::Yellow());
		pt_obj->Set_Color(galaxy_driver.Color(galaxy_driver.galaxy.vel[i].norm()));
		pt_obj->point_size = galaxy_driver.galaxy.radius[i];
		pt_obj->Set_Data_Refreshed();
		pt_obj->Update_Data_To_Render();
	}
}

void FinalProjectDriver::Cancel_Galaxy(void)
{
	for (int i = 0; i < galaxy_driver.number; i++) {
		auto pt_obj = dynamic_cast<OpenGLPoint*>((opengl_window->object_list[i + galaxy_start_idx]).get());
		pt_obj->visible = false;

	}
}


void FinalProjectDriver::Update_Scene1(int frame_cnt)
{
	opengl_window->camera_target = camera_pos1;
	glm::vec3 pos(camera_pos1[0], camera_pos1[1], camera_pos1[2]);
	glm::vec3 front = -glm::normalize(pos);
	glm::vec3 up(0, 1, 0);
	glm::vec3 right = glm::cross(front, up);
	up = glm::cross(right, front);
	opengl_window->camera_front = front;
	opengl_window->camera_up = up;

	spacecraft->visible = false;
	real factor = 0.1;
    marsMesh->model_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(factor));
    sunMesh->model_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(factor));

	galaxy_driver.Advance_Step(1.0 / FPS);
	Sync_Galaxy();
}

void FinalProjectDriver::Update_Scene2(int frame_cnt)//frame from the start of scene 2
{
	//scene 2: 10s, static camera, spaceship moving before it
	float now_time = (frame_cnt + 0.0) / FPS;

	opengl_window->camera_target = camera_pos1;
	glm::vec3 pos(camera_pos1[0], camera_pos1[1], camera_pos1[2]);
	glm::vec3 front = -glm::normalize(pos);
	glm::vec3 up(0, 1, 0);
	glm::vec3 right = glm::cross(front, up);
	up = glm::cross(right, front);
	opengl_window->camera_front = front;
	opengl_window->camera_up = up;

	spacecraft->visible = true;

	glm::vec3 ship_move_vec(0,0,0);

	if (now_time<up_time){
		float now_move_height = (spaceship_up_end - spaceship_up_start) * (now_time / up_time);
		ship_move_vec= up*now_move_height +up * (-1.f) * spaceship_down_len + front * spaceship_forward_len;
	}
	else if (up_time+stay_time>=now_time && now_time >=up_time){
		ship_move_vec= up * (-1.f) * (spaceship_down_len - (spaceship_up_end - spaceship_up_start) )+ front * spaceship_forward_len;
	}
	else{
		float now_move_length = spaceship_forward_start + (spaceship_forward_end - spaceship_forward_start) * ((now_time-up_time-stay_time) / (time_2-up_time-stay_time));
		ship_move_vec = front * now_move_length + up * (-1.f) * (spaceship_down_len - (spaceship_up_end - spaceship_up_start) )+ front * spaceship_forward_len;
	}
	glm::mat4 trans = glm::mat4(1.0f);

		
	// up = vec3(0, 1, 0.5);
	// right = glm::cross(front, up);
	// up = glm::cross(right, front);


	//they happen from left to right
	trans = glm::translate(trans, pos + ship_move_vec);//2nd operation
	real fact = 1.0 / spaceship_scale;
	trans = glm::scale(trans, glm::vec3(fact, fact, fact));//1st operation
	trans=trans* glm::mat4(
		-right[0],-right[1],-right[2], 0.f,		////column 0
		-front[0],-front[1],-front[2], 0.f,		////column 1
		up[0],up[1],up[2], 0.f,		////column 2
		0.f, 0.f, 0.f, 1.f);		////column 3	////set the translation in the last column
	
	spacecraft->model_matrix = trans;

	galaxy_driver.Advance_Step(1.0/FPS);
	Sync_Galaxy();
}

void FinalProjectDriver::Update_Scene3(int frame_cnt)
{
	//scene 3: 5s, transition, spaceship moving along with camera
	float now_time = (frame_cnt + 0.0) / FPS;

	float now_move_length = spaceship_intenseforward_start + (spaceship_intenseforward_end - spaceship_intenseforward_start) * (now_time / time_3);

	//opengl_window->camera_target = camera_pos1;
	glm::vec3 pos(camera_pos1[0], camera_pos1[1], camera_pos1[2]);
	glm::vec3 front = -glm::normalize(pos);
	glm::vec3 up(0, 1, 0);
	glm::vec3 right = glm::cross(front, up);
	up = glm::cross(right, front);
	opengl_window->camera_front = front;
	opengl_window->camera_up = up;

	glm::vec3 move = front*now_move_length;
	opengl_window->camera_target = camera_pos1+ Vector3f(move[0],move[1],move[2]);


	glm::vec3 ship_move_vec = front * (spaceship_forward_end+spaceship_forward_len+now_move_length) + up * (-1.f) * (spaceship_down_len - (spaceship_up_end - spaceship_up_start) );

	float time_angle =  2;
	float angle = 0;
	if (now_time<time_angle ){
		angle = 0.5*now_time/time_angle;
	}
	else {
		angle = 0.5-0.8* (now_time-time_angle)/ (time_3-time_angle);
	}

	
	up = vec3(angle, 1, 0);
	right = glm::cross(front, up);
	up = glm::cross(right, front);


	glm::mat4 trans = glm::mat4(1.0f);
	//they happen from left to right
	trans = glm::translate(trans, pos + ship_move_vec);//2nd operation
	real fact = 1.0 / spaceship_scale;
	trans = glm::scale(trans, glm::vec3(fact, fact, fact));//1st operation
	trans=trans* glm::mat4(
		-right[0],-right[1],-right[2], 0.f,		////column 0
		-front[0],-front[1],-front[2], 0.f,		////column 1
		up[0],up[1],up[2], 0.f,		////column 2
		0.f, 0.f, 0.f, 1.f);		////column 3	////set the translation in the last column

	spacecraft->model_matrix = trans;

    real factor = 1.1f * (now_time / time_3);
    marsMesh->model_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(factor));
    sunMesh->model_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(factor));


	galaxy_driver.Advance_Step(1.0/FPS);
	Sync_Galaxy();
}

void FinalProjectDriver::Update_Scene4(int frame_cnt)
{
	//scene 4: 30s, spaceship sailing in a solar system, along with camera
	float now_time = (frame_cnt + 0.0) / FPS;



	//opengl_window->camera_target = camera_pos1;
	glm::vec3 pos(camera_pos1[0], camera_pos1[1], camera_pos1[2]);

	glm::vec3 front = -glm::normalize(pos);
	front = glm::normalize(front);
	glm::vec3 up(0, 1, 0);
	glm::vec3 right = glm::cross(front, up);
	up = glm::cross(right, front);
	opengl_window->camera_front = front;
	opengl_window->camera_up = up;

	float time_angle =  10;
	float angle = 0;

	float now_move_length = 0.0;
	float now_moveright_length = 0.0;

	if (now_time<time_angle ){
		angle = -0.3+0.8*now_time/time_angle;
		now_move_length = spaceship_last_start+ (spaceship_last_end - spaceship_last_start) * (now_time / time_angle);
		now_moveright_length = spaceship_lastright_start+ (spaceship_lastright_end - spaceship_lastright_start) * (now_time / time_angle);
	}
	else {
		angle = 0.5-1* (now_time-time_angle)/ (time_4-time_angle);
		now_move_length = spaceship_last_end+spaceship_last_start+ (spaceship_last_end - spaceship_last_start) * (now_time-time_angle)/ (time_4-time_angle);
		now_moveright_length =  spaceship_lastright_end-(spaceship_lastright_start+ 2.0*(spaceship_lastright_end - spaceship_lastright_start) * (now_time-time_angle)/ (time_4-time_angle));
		
	}

   
	
	glm::vec3 move = front*(now_move_length+spaceship_intenseforward_end ) + right*now_moveright_length ;
	opengl_window->camera_target = camera_pos1+ Vector3f(move[0],move[1],move[2]);


	glm::vec3 ship_move_vec =  right*now_moveright_length +front * (spaceship_forward_end+spaceship_forward_len+spaceship_intenseforward_end+now_move_length) + up * (-1.f) * (spaceship_down_len - (spaceship_up_end - spaceship_up_start) );



	up = vec3(angle, 1, 0);
	right = glm::cross(front, up);
	up = glm::cross(right, front);

	glm::mat4 trans = glm::mat4(1.0f);
	//they happen from left to right
	trans = glm::translate(trans, pos + ship_move_vec);//2nd operation
	real fact = 1.0 / spaceship_scale;
	trans = glm::scale(trans, glm::vec3(fact, fact, fact));//1st operation
	trans=trans* glm::mat4(
		-right[0],-right[1],-right[2], 0.f,		////column 0
		-front[0],-front[1],-front[2], 0.f,		////column 1
		up[0],up[1],up[2], 0.f,		////column 2
		0.f, 0.f, 0.f, 1.f);		////column 3	////set the translation in the last column

	spacecraft->model_matrix = trans;

	galaxy_driver.Advance_Step(1.0/FPS);
	Sync_Galaxy();
}


void FinalProjectDriver::Update_Frame()
{
	opengl_window->texts["frame"] = "Frame: " + std::to_string(frame);
	for (auto& obj : opengl_window->object_list) {
		if (!obj->interactive)obj->Refresh(frame);
		obj->Update_Data_To_Render();
		//std::cout << "object: " << obj->name << "\n";
	}
	if (opengl_window->display_offscreen) {
		opengl_window->frame_offscreen = frame;
		opengl_window->frame_offscreen_rendered = -1;
	}
	std::cout << "frame= " << frame << "\n";
	int offset = 0;
	if (frame < offset + time_1 * FPS) {
		//scene 1: 20s, static camera, glaring at galaxy
		Update_Scene1(frame - offset);
		return;
	}
	offset += time_1 * FPS;
	if (frame < offset + time_2 * FPS) {
		//scene 2: 10s, static camera, spaceship moving before it
		Update_Scene2(frame - offset);
		return;
	}
	offset += time_2 * FPS;
	if (frame < offset + time_3 * FPS) {
		//scene 3: 5s, transition, spaceship moving along with camera
		Update_Scene3(frame - offset);
		return;
	}
	offset += time_3 * FPS;
	if (frame < offset+time_4 * FPS) {
		//scene 4: 30s, spaceship sailing in a solar system, along with camera
		Update_Scene4(frame - offset);
		return;
	}
	offset+= time_4 * FPS;
	system("pause");
	//real time = Time_At_Frame(frame);
	//opengl_window->camera_target = Vector3f(0, 0, time);
}

#endif

