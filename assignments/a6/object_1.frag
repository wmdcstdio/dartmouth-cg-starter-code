/*This is your fragment shader for texture and normal mapping*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

uniform mat4 model;						////model matrix

in vec3 vtx_pos;		////vertex position in the world space
in vec3 vtx_normal;
in vec2 vtx_uv;
in vec3 vtx_tang;

out vec4 frag_color;

/*This part is the same as your previous assignment. Here we provide a default parameter set for the hard-coded lighting environment. Feel free to change them.*/
const vec3 LightPosition = vec3(3, 1, 10);

const vec3 LightIntensity = vec3(2);
const vec3 ka = 0.3*vec3(1., 1., 1.);
const vec3 kd = 2*vec3(1., 1., 1.);
const vec3 ks = vec3(2.);
const float n = 400.0;

float distance(vec3 a, vec3 b) {return sqrt(dot(a-b, a-b));}
void main()
{
	vec3 vtx_pos2 = (model * vec4(vtx_pos.rgb, 1.)).rgb;
	vec3 dir=normalize(LightPosition-vtx_pos2);
	vec4 texture_col=texture(tex_albedo, vtx_uv);
	vec3 normal = texture(tex_normal, vtx_uv).rgb;
	normal = normalize(normal * 2.0 - 1.0);
	vec3 vtx_bitang=cross(vtx_normal, vtx_tang);
	mat3 TBN=mat3(vtx_tang,vtx_bitang,vtx_normal);
	normal=normalize(TBN*normal);
	float dist = distance(vtx_pos2, LightPosition);
	vec4 shading_col = vec4(ka+kd * 5. / dist * LightIntensity*max(0,dot(normal,dir)),1.0);
//	vec4 shading_col = vec4(ka+kd * LightIntensity*max(0,dot(normal,dir)),1.0);
	vec4 col=texture_col*shading_col;
	frag_color = vec4(col.rgb, 1);
//	frag_color = vec4(0.);
}