/*This is your first fragment shader!*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
//// TODO: declare the input fragment attributes here
in vec2 vert_uv;

/*output variables*/
out vec4 frag_color;

void main()							
{						
	vec3 col = vec3(1.0);
	int ui=int(vert_uv.x*10)%2;
	int vi=int(vert_uv.y*10)%2;
	float alpha=float(bool(ui)^^bool(vi));
	frag_color=vec4(col*alpha,1.0);
}	